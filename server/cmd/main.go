package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	"gitlab.com/vund5/chat-application/server/chat"
)

var addr = flag.String("addr", ":8080", "http service address")

func main() {
	hub := chat.NewHub()
	go hub.Run()

	fmt.Print("Created Hub successfully !\n")
	mydir, _ := os.Getwd()
	var dir = mydir + "/build"
	fileServe := http.FileServer(http.Dir(dir))
	http.Handle("/", http.StripPrefix(strings.TrimRight("/", "/"), fileServe))
	fmt.Println(dir)
	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		chat.ServeWs(hub, w, r)
	})

	err := http.ListenAndServe(*addr, nil)
	if err != nil {
		log.Fatalf("ListenAndServe: %v", err)
	}
	fmt.Println("Ready")
}
