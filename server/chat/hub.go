package chat

// Hub maintains the set of active clients and broadcasts messages to the
// clients.
type Hub struct {
	// Registered clients.
	clients map[*Client]bool

	// Inbound messages from the clients.
	broadcast chan []byte

	// Register requests from the clients.
	register chan *Client

	room map[string]*Room
}

type Room struct {
	room_id        string
	room_broadcast chan []byte
	members        []*Client
}

func (r *Room) doBroadcast(message []byte) {
	for _, member := range r.members {
		member.send <- message
	}
}

func NewHub() *Hub {
	return &Hub{
		broadcast: make(chan []byte),
		register:  make(chan *Client),
		clients:   make(map[*Client]bool),
		room:      make(map[string]*Room),
	}
}

func (h *Hub) Run() {
	for {
		select {
		case client := <-h.register:
			h.clients[client] = true
		case message := <-h.broadcast:
			for client := range h.clients {
				select {
				case client.send <- message:
				default:
					close(client.send)
					delete(h.clients, client)
				}
			}
		}
	}
}
